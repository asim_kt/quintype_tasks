# How to develop and run the project
1. Install less: (One time thing)
`npm install`
2. Make changes
3. Compile and minify *less* to *css* if you made changes to *.less* file(s)
`lessc less/style.less --autoprefix --clean-css="--advanced --compatibility=ie8" css/style.css`
4. Open or serve */index.html* in browser.


**NOTE: I'm tracking *css/style.css* which will be overrided when we run `lessc`. So no need to change the *css/style.css* file.**