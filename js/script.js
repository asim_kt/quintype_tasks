"use strict";
document.getElementById("majorArticle").addEventListener("click", displayDetails);
document.getElementById("close").addEventListener("click", closeDetails);
document.getElementById("modalContent").addEventListener("click", preventEventBubbling);
document.getElementById("modal").addEventListener("click", closeDetails);
document.onkeydown = function(evt) {
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    } else {
        isEscape = (evt.keyCode == 27);
    }
    if (isEscape) {
        closeDetails();
    }
};

function displayDetails() {
	document.getElementById("modal").style.display = "block";
}

function closeDetails() {
	document.getElementById("modal").style.display = "none";
}

function preventEventBubbling(e) {
	e.preventDefault();
	e.stopPropagation();
	window.event.cancelBubble = true
}